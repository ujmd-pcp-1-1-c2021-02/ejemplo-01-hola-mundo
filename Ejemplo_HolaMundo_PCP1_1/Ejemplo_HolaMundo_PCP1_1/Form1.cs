using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_HolaMundo_PCP1_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo de PCP 1-1.";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo de PCP 1-1.";
        }
    }
}
